export const environment = {
  production: false,
  apiUrl:"https://api.viciny.com",
  url:"https://viciny.com",
  apiKeyMap:"pk.eyJ1IjoidmljaW55IiwiYSI6ImNrOG5qNjNicDB6OTIzaHFvZzczNG5xOTYifQ.dXqucMWbHc55Ayw4vMF7FA",
  firebase: {
    apiKey: "AIzaSyBFXRneBU6tQ5ASJR_8XV9GBoHt5-XceuE",
    authDomain: "viciny-fbbc4.firebaseapp.com",
    databaseURL: "https://viciny-fbbc4.firebaseio.com",
    projectId: "viciny-fbbc4",
    storageBucket: "viciny-fbbc4.appspot.com",
    messagingSenderId: "457753507369",
    appId: "1:457753507369:web:8a82db305f906e890dce38",
    measurementId: "G-5P9HHGHMDM"
  },
  epayco:{
    key: '45b960805ced5c27ce34b1600b4b9f54',
    test: true
  }
};
