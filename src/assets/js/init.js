function init_all() {
    init_jquery();
    init_popper();
    init_bmd();
    init_moment();
    init_bdtp();
    init_nouislider();
    init_bti();
    init_bsp();
    init_jb();
    init_mk();
}