import { Component, OnInit } from '@angular/core';
import { Router, ActivationEnd } from '@angular/router';
import { filter, map } from 'rxjs/operators';

// =========================================
// SERVICES
// =========================================
import {
  AuthService,
  UserService,
  UiService,
} from '../../services/services.index';

// =========================================
// UTILITIES
// =========================================
declare function init_all();

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  isLogin: boolean;
  isAdmin = false;
  image = '';
  user = null;
  imageProfile = '/assets/images/user.svg';

  constructor(
    private authService: AuthService,
    private userService: UserService,
    private uiService: UiService,
    private router: Router,
  ) {
    this.getDataRoute().subscribe(res => {
      this.image = res.imgLogo;
    });
    this.isLogin = authService.isLogin();
    this.user = this.authService.user;
    init_all();
    if (this.isLogin) {
      this.roles();
      this.userService.getUser().then(
        user => {
          init_all();
          this.user = this.userService.user;
          if (user.profile !== null) {
            if (user.profile.photo !== null) {
              this.imageProfile = user.profile.photo;
            }
          }
        }
      ).catch(err => {
        init_all();
        this.user.email_verified_at = null;
        this.userService.resendEmail().subscribe(res => {
            this.uiService.showWarning("El usuario no se encuentra verificado. Hemos enviado un correo electrónico de verificación por favor confirmalo! IMPORTANTE: Muchas veces el mensaje puede estar en la sección de SPAM.");
          }
        );
      });
    }
  }

  ngOnInit() {
    init_all();
  }

  // This method is responsible for obtaining data variables sent by the url
  getDataRoute() {
    return this.router.events.pipe(
      filter(event => event instanceof ActivationEnd),
      filter((event: ActivationEnd) => event.snapshot.firstChild === null),
      map((event: ActivationEnd) => event.snapshot.data)
    );
  }

  // This method is responsible for closing the user session.
  logout() {
    this.authService.logout();
    this.isLogin = false;
  }

  // This method is responsible for knowing if the auth user is admin or not
  roles() {
    this.authService.roles().subscribe(
      (res: any) => {
        res.indexOf('administrator') !== -1 ? this.isAdmin = true : this.isAdmin = false;
      },
      err => {
        this.isAdmin = false;
      }
    );
  }

  openNav() {
    document.getElementById('mySidenav').style.width = '300px';
  }

  closeNav() {
    document.getElementById('mySidenav').style.width = '0';
  }

  openDropdown() {
    document.getElementById('myDropdown').classList.toggle('show');
  }
}
