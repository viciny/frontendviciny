import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Title } from '@angular/platform-browser';
import { AuthService } from '../../services/api/auth.service';

// =========================================
// SERVICES
// =========================================
import {
  UiService
} from '../../services/services.index';

// =========================================
// UTILITIES
// =========================================
declare function init_all();

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  form: FormGroup;

  constructor(
    private authService: AuthService,
    private router: Router,
    private uiService: UiService,
    private titleRoute: Title
  ) {
    init_all();
    this.form = new FormGroup({
      name: new FormControl(null, [Validators.required]),
      email: new FormControl(null, [Validators.required, Validators.email]),
      email_confirmation: new FormControl(null, [Validators.required, Validators.email]),
      password: new FormControl(null, Validators.required),
      password_confirmation: new FormControl(null, Validators.required)
    }, { validators: [this.verifyPass, this.verifyEmail] });
    this.form.setValue({
      name: '',
      email: '',
      email_confirmation: '',
      password: '',
      password_confirmation: ''
    });
  }

  // This method is responsible for verifying that two pass are equal
  verifyPass(group: FormGroup) {
    return group.get('password').value === group.get('password_confirmation').value ? null : { verifyPass: true };
  }

  // This method is responsible for verifying that two email are equal
  verifyEmail(group: FormGroup) {
    return group.get('email').value === group.get('email_confirmation').value ? null : { verifyEmail: true };
  }

  ngOnInit() {
    init_all();
    this.titleRoute.setTitle('Viciny - Registro de usuario');
  }

  // This method is responsible for calling the service to login a user
  register() {
    if (this.form.invalid) {
      this.uiService.showWarning('Formulario invalido!');
      return;
    }
    this.uiService.loading();
    this.authService.register(this.form.value).subscribe(res => {
      this.uiService.closeLoading();
      this.router.navigate(['/inicio']);
      this.uiService.showSuccess('Usuario creado correctamente!');
    }, err => {
      this.uiService.showError(err, 'Error al crear el usuario');
    });
  }
}
