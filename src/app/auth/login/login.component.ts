import { Component, OnInit, NgZone } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Title } from '@angular/platform-browser';

// =========================================
// SERVICES
// =========================================
import {
  UiService,
  AuthService
} from '../../services/services.index';

// =========================================
// UTILITIES
// =========================================
declare function init_all();

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['../login/login.component.css']
})
export class LoginComponent implements OnInit {

  form: FormGroup;
  emailResetPass: string;

  constructor(
    public router: Router,
    private titleRoute: Title,
    private uiService: UiService,
    private auth: AuthService,
    private ngZone: NgZone
  ) {
    init_all();
    this.loadDataLogin();
  }

  ngOnInit() {
    init_all();
    this.titleRoute.setTitle('Viciny - Inicio de sesión');
  }

  // This method is responsible for calling the service to login a user
  login() {
    if (this.form.invalid) {
      this.uiService.showWarning('Formulario invalido!');
      return;
    }
    this.uiService.loading();
    this.auth.login(this.form.value).subscribe(res => {
      this.uiService.closeLoading();
      this.router.navigate(['/inicio']);
      this.uiService.showSuccess('Bienvenido a Viciny!');
    }, err => {
      this.uiService.showError(err, 'Error al iniciar sesión');
    });
  }

  // This method is responsible for calling the service to login a user by social network
  socialLogin(provider: string ) {
    this.uiService.loading();
    this.auth.loginFirebase(provider).then(user => {
      this.uiService.closeLoading();
        this.ngZone.run(() => {
          this.router.navigateByUrl('/inicio');
          init_all();
        });
      }
    ).catch(
      err => {
        this.uiService.showError(err, 'Error al iniciar sesión');
      }
    );
  }

  // This method is responsible for calling the service to forgot password
  forgotPassword(form: FormGroup) {
    if (form.invalid) {
      this.uiService.showWarning('Formulario invalido!');
      return;
    }
    this.uiService.loading();
    this.auth.resetPassword(form.value).subscribe(res => {
      this.uiService.closeLoading();
      this.router.navigate(['/login']);
      this.uiService.showSuccess(res.message);
    }, err => {
      this.uiService.showError(err, 'Error al enviar el email');
    });
  }

  // This methos is responsible for load variables to login
  loadDataLogin() {
    this.form = new FormGroup({
      email: new FormControl(null, [Validators.required, Validators.email]),
      password: new FormControl(null, Validators.required)
    });
    this.form.setValue({
      email: '',
      password: ''
    });
  }
}
