import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

// =========================================
// SERVICES
// =========================================
import {
  AdminService,
  UiService
} from './../../services/services.index';

// =========================================
// COMPONENTS
// =========================================
import { OptionsTickeTableComponent } from './options-ticke-table/options-ticke-table.component';

// =========================================
// UTILITIES
// =========================================
declare function init_all();

@Component({
  selector: 'app-admin-tickes',
  templateUrl: './admin-tickes.component.html',
  styleUrls: ['./admin-tickes.component.css']
})
export class AdminTickesComponent implements OnInit {

  settings = {
    columns: {
      id: {
        title: 'ID'
      },
      created_at: {
        title: 'Fecha de adquisición'
      },
      state: {
        title: 'Estado'
      },
    },
    actions: {
      columnTitle: null,
      add: false,
      edit: false,
      delete: false
    },
  };

  data = [];

  constructor(
    private rutaActiva: ActivatedRoute,
    private adminService: AdminService,
    private uiService: UiService,
  ) {
    init_all();
  }

  ngOnInit() {
    init_all();
    this.adminService.getTickets(this.rutaActiva.snapshot.params.tickets_order_id)
    .subscribe(
      (tickes: any) => {
        this.data = tickes.map((ticket: any) => {
          ticket.state = ticket.state === 1 ? 'Sin usar' : 'Usado';
          return ticket;
        });
      }
    );
  }

  // This method is responsible for deleting a event.
  deleteEvent(ticketId) {
    this.adminService.deleteTicket(ticketId).subscribe(
      ticket => {
        this.data = this.data.filter((ticket: any) => {
          return ticket.id !== ticketId;
        });
      },
      err => {
        this.uiService.showError(err, 'Error al eliminar el ticket');
      }
    );
  }
}
