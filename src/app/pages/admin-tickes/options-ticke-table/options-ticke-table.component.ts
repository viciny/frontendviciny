import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-options-ticke-table',
  templateUrl: './options-ticke-table.component.html',
  styleUrls: ['./options-ticke-table.component.css']
})
export class OptionsTickeTableComponent implements OnInit {

  @Input() rowData: any;
  @Output() delete = new EventEmitter();

  constructor() { }

  ngOnInit() {
  }

  // This method is responsible for deleting a ticket.
  deleteTicket() {
    this.delete.emit(this.rowData);
  }
}
