import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';

// =========================================
// MODELS - USER
// =========================================
import { User } from './../../models/user';

// =========================================
// SERVICES
// =========================================
import {
  UiService,
  UserService,
  BusinessService
} from '../../services/services.index';

// =========================================
// UTILITIES
// =========================================
declare var $: any;
declare function init_all();

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {

  form: FormGroup;
  typesEvent = [];
  user: User = new User();
  deparments: Array<any> = [];
  municipalities: Array<any> = [];
  file: File = null;
  imageTemp: string | ArrayBuffer = '/assets/images/user.svg';
  imageText: any = 'Selecciona la imagen';

  constructor(
    private uiService: UiService,
    private bussinesService: BusinessService,
    private userService: UserService
  ) {
    init_all();
    this.loadData();
    this.loadUSer();
    this.bussinesService.getTypeEvent().toPromise().then((typesEvent: any) => {
      this.typesEvent = typesEvent.data;
      this.loadingFavoriteTypeEventsCheckbox();
    });
  }

  ngOnInit() {
    init_all();
    this.loadDepartments();
  }

  // This method is responsible for loading the user.
  loadUSer() {
    this.userService.getUser().then(
      (user: any) => {
        this.user = user;
        this.form.patchValue({
          name: this.user.name,
        });
        this.loadProfile();
      }
    );
  }

  // This method is responsible for loading the user's profile.
  loadProfile() {
    if (this.user.profile != null) {
      this.form.patchValue({
        type_document: this.user.profile ? this.user.profile.type_document : '',
        nit: this.user.profile ? this.user.profile.nit : '',
        phone: this.user.profile ? this.user.profile.phone : '',
        address: this.user.profile ? this.user.profile.address : '',
        account_bank: this.user.profile ? this.user.profile.account_bank : '',
        bank_name: this.user.profile ? this.user.profile.bank_name : '',
        department_id: this.user.profile ? this.user.profile.municipality.department_id : '',
        municipality_id: this.user.profile ? this.user.profile.municipality.id : '',
      });
      if (this.user.profile.municipality) {
        this.loadMunicipalities(this.user.profile.municipality.department_id);
      }
      if (this.user.profile.photo) {
        this.imageTemp = this.user.profile.photo;
      }
    }
  }

  // This method is responsible for calling the update service to user
  update() {
    if (!this.form.invalid) {
      this.bussinesService.addFavoriteTypeEvent(this.loadingNewFavoriteTypeEvents()).toPromise();
      this.uiService.loading();
      this.userService.updateProfile(this.form.value).toPromise().then(
        (res: any) => {
          if (this.file) {
            this.onUpload(res.user.id);
          } else {
            this.uiService.closeLoading();
          }
        }
      ).catch(
        err => {
          this.uiService.showError(err, 'Error al actualizar el perfil');
        }
      );
    }
  }

  // This method is responsible for loading the selected image
  onFileSelected(event) {
    this.file = <File>event.target.files[0];
    const reader = new FileReader();
    const urlImageTemp = reader.readAsDataURL(<File>event.target.files[0]);
    reader.onloadend = () => {
      this.imageTemp = reader.result;
    };
    this.imageText = <File>event.target.files[0].name;
  }

  // This method is responsible for uploading the selected image
  async onUpload(eventId) {
    const fd = new FormData();
    fd.append('photo', this.file);
    this.userService.addPhoto(fd).subscribe(res => {
      this.imageTemp = res.photo;
      this.uiService.closeLoading();
    }, err => {
      this.uiService.showError(err, 'Error al cargar la imagen');
    });
  }

  // This method is responsible for loading new types of favorite events
  loadingNewFavoriteTypeEvents() {
    const searchIDs = [];
    $('input:checkbox:checked').each(function() {
      searchIDs.push($(this).val());
    });
    return searchIDs;
  }

  // This method is responsible for loading new types of favorite events check
  loadingFavoriteTypeEventsCheckbox() {
    this.bussinesService.getFavoriteTypeEvent().toPromise().then(
      (res: any) => {
        res.forEach(
          (type: any) => {
            $('#favoriteTypeEvent_' + type.id).prop('checked', true);
          }
        );
      }
    );
  }

  // This method is responsible for obtaining the departments
  loadDepartments() {
    this.bussinesService.getDepartments().subscribe((res: any) => {
      this.deparments = res;
      if (this.deparments.length > 0) {
        this.loadMunicipalities(this.deparments[0].id);
      }
    }, err => {
      this.uiService.showError(err, 'Error al cargar los departamentos');
    });
  }

  // This method is responsible for obtaining the municipalities
  loadMunicipalities(departmentId) {
    if (departmentId) {
      this.bussinesService.getMinicipalities(departmentId).subscribe((res: any) => {
        this.municipalities = res;
      }, err => {
        this.uiService.showError(err, 'Error al cargar los municipios');
      });
    } else {
      this.municipalities = [];
    }
  }

  // This method is responsible for loading the data for the update user.
  loadData() {
    this.form = new FormGroup({
      name: new FormControl(null, [Validators.required]),
      type_document: new FormControl(null, [Validators.required]),
      nit: new FormControl(null, [Validators.required]),
      phone: new FormControl(null, []),
      address: new FormControl(null, [Validators.required]),
      account_bank: new FormControl(null, []),
      bank_name: new FormControl(null, []),
      department_id: new FormControl(null, [Validators.required]),
      municipality_id: new FormControl(null, [Validators.required]),
    });
    this.form.setValue({
      name: '',
      type_document: '',
      nit: '',
      phone: '',
      address: '',
      bank_name: '',
      account_bank: '',
      department_id: '',
      municipality_id: ''
    });
  }

}
