import { Component, OnInit, Input } from '@angular/core';

// =========================================
// SERVICES
// =========================================
import {
  BusinessService,
  UiService
} from '../../../services/services.index';

@Component({
  selector: 'app-modal-edit-personal-event',
  templateUrl: './modal-edit-personal-event.component.html',
  styleUrls: ['./modal-edit-personal-event.component.css']
})
export class ModalEditPersonalEventComponent implements OnInit {

  @Input() event: any;
  file: File = null;
  imageTemp: string | ArrayBuffer = '/assets/images/stage.svg';
  imageText: any = 'Selecciona la imagen';

  constructor(
    private businessService: BusinessService,
    private uiService: UiService
  ) { }

  ngOnInit() {
  }

  // This method is responsible for displaying the selected image
  onFileSelected(file: File) {
    this.file = file;
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onloadend = () => {
      this.event.image = reader.result;
    };
    this.imageText = file.name;
  }

  // This method is responsible for updating the event.
  update() {
    this.businessService.updateEvent(this.event, this.event.id).subscribe(
      (res: any) => {
        if (this.file != null) {
          this.onUpload(res.data.id);
        } else {
          this.uiService.showSuccess('Evento actualizado con éxito!');
        }
      }, err => {
        this.uiService.showError(err, 'Error al actualizar el evento');
      }
    );
  }

  // This method is responsible for uploading the selected image
  onUpload(eventId) {
    const fd = new FormData();
    fd.append('image', this.file);
    this.businessService.addImageEvent(eventId, fd).subscribe((res: any) => {
      this.event.image = res.data.image;
      this.uiService.showSuccess('Evento actualizado con éxito!');
    }, err => {
      this.uiService.showError(err, 'Error al cargar la imagen');
    }
    );
  }
}
