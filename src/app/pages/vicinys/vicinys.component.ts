import { Component, OnInit } from '@angular/core';
import Swal from 'sweetalert2';

// =========================================
// MODEL - EVENT
// =========================================
import { Event } from '../../models/event';

// =========================================
// SERVICES
// =========================================
import {
  BusinessService,
  UiService
} from '../../services/services.index';

// =========================================
// UTILITIES
// =========================================
declare function init_all();

@Component({
  selector: 'app-vicinys',
  templateUrl: './vicinys.component.html',
  styleUrls: ['./vicinys.component.css']
})
export class VicinysComponent implements OnInit {

  events: Array<Event> = [];
  selectedEvent: Event = new Event();

  constructor(
    private businessService: BusinessService,
    private uiService: UiService
  ) {
    init_all();
    this.loadEvents();
  }

  ngOnInit() {
    init_all();
  }

  // This method is responsible for calling the event load
  loadEvents() {
    this.uiService.loading();
    this.businessService.getEventsCreated().subscribe((res: any) => {
      this.events = res.data;
      this.uiService.closeLoading();
    }, err => {
      this.uiService.showError(err, 'Error al cargar los vicinys');
    });
  }

  // This method is responsible for delete a event
  deleteEvent(eventId) {
    this.businessService.deleteEvent(eventId).subscribe((res: any) => {
        this.events = this.events.filter(event => {
          return event.id !== eventId;
        });
        Swal.fire(
          'Viciny eliminado!',
          'Tu viciny ha sido eliminado correctamente.',
          'success'
        );
      }, err => {
        this.uiService.showError(err, 'Error al eliminar el viciny');
      });
  }

  // This method is responsible for show alert to delete a event
  deletEventAlert(eventId) {
    Swal.fire({
      title: '¿Estás seguro?',
      text: 'No podrás revertir esto!',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#5724A1',
      cancelButtonColor: '#f44336',
      confirmButtonText: 'Si, eliminar!',
      cancelButtonText: 'No, mantenerlo!'
    }).then((result) => {
      if (result.value) {
        this.deleteEvent(eventId);
      }
    });
  }

  // This method is responsible for disploying a event
  selectEVent(event) {
    this.selectedEvent = event;
  }
}
