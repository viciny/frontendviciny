import { Component, OnInit } from '@angular/core';

// =========================================
// SERVICES
// =========================================
import {
  BusinessService,
  UiService
} from '../../services/services.index';

// =========================================
// MODEL - EVENT
// =========================================
import { Event } from '../../models/event';

// =========================================
// UTILITIES
// =========================================
declare var $: any;
declare var moment: any;
declare function init_all();

@Component({
  selector: 'app-welcome',
  templateUrl: './welcome.component.html',
  styleUrls: ['./welcome.component.css']
})
export class WelcomeComponent implements OnInit {

  typesEvent: Array<any> = [];
  eventsPopular: Array<Event> = [];
  events: Array<Event> = [];
  eventsWeekend: Array<Event> = [];
  eventsFree: Array<Event> = [];

  selectTypeEvent = 'todos';
  date = 'any_date';
  municipality = 'Bogota';

  constructor(
    private businessService: BusinessService,
    private uiService: UiService
  ) {
    init_all();
    this.loadtypeEvents();
    this.loadEventsPopulars();
  }

  ngOnInit() {
    init_all();
    $('#carouselExampleIndicators').carousel();
    this.searchEvent();
    this.selectDatetime();
  }

  // This method is responsible for conducting an event search
  searchEvent() {
    const query = {};
    const dateQuery = this.transformDateQuery();
    if (dateQuery.start_date !== null) {
      query['start_date'] = dateQuery.start_date;
    }
    if (dateQuery.finish_date !== null) {
      query['finish_date'] = dateQuery.finish_date;
    }
    if (this.municipality !== '') {
      query['municipality'] = this.municipality;
    }
    if (this.selectTypeEvent !== 'todos') {
      query['type_event_id'] = this.selectTypeEvent;
    }

    this.businessService.searchEvents(query).toPromise().then(
      (res: any) => {
        this.events = res.data;
      }
    );
    this.loadingEventsFree();
    this.loadingEventsWeekend();
  }

  // This method is responsible for loading events for free.
  loadingEventsFree() {
    const query = {};
    const dateQuery = this.transformDateQuery();
    query['ticket_value'] = 0;
    if (dateQuery.start_date !== null) {
      query['start_date'] = dateQuery.start_date;
    }
    if (dateQuery.finish_date !== null) {
      query['finish_date'] = dateQuery.finish_date;
    }
    if (this.municipality !== '') {
      query['municipality'] = this.municipality;
    }
    if (this.selectTypeEvent !== 'todos') {
      query['type_event_id'] = this.selectTypeEvent;
    }

    this.businessService.searchEvents(query).toPromise().then(
      (res: any) => {
        this.eventsFree = res.data;
      }
    );
  }

  // This method is responsible for loading weekend events
  loadingEventsWeekend() {
    const this_weekend = 7 - moment().day();
    const query = {
      start_date: moment().add(this_weekend - 2 >= 0 ? this_weekend - 2 : 0, 'day').format('YYYY/MM/DD'),
      finish_date: moment().add(this_weekend, 'day').format('YYYY/MM/DD'),
    };
    if (this.municipality !== '') {
      query['municipality'] = this.municipality;
    }
    this.businessService.searchEvents(query).toPromise().then(
      (res: any) => {
        this.eventsWeekend = res.data;
      }
    );
  }

  // This method is responsible for transforming the date
  transformDateQuery() {
    const query = {
      start_date: null,
      finish_date: null
    };
    switch (this.date) {
      case 'today':
        query.start_date = moment().format('YYYY/MM/DD');
        query.finish_date = moment().add(1, 'day').format('YYYY/MM/DD');
        break;
      case 'tomorrow':
        query.start_date = moment().add(1, 'day').format('YYYY/MM/DD');
        query.finish_date = moment().add(2, 'day');
        break;
      case 'this_weekend':
        let this_weekend = 7 - moment().day();
        query.start_date = moment().add(this_weekend - 2 >= 0 ? this_weekend - 2 : 0, 'day').format('YYYY/MM/DD');
        query.finish_date = moment().add(this_weekend, 'day').format('YYYY/MM/DD');
        break;
      case 'this_week':
        let this_week = 7 - moment().day();
        query.start_date = moment().format('YYYY/MM/DD');
        query.finish_date = moment().add(this_week + 1, 'day').format('YYYY/MM/DD');
        break;
      case 'next_week':
        let next_week = 7 - moment().day();
        query.start_date = moment().add(next_week + 1, 'day').format('YYYY/MM/DD');
        query.finish_date = moment().add(next_week + 8, 'day').format('YYYY/MM/DD');
        break;
      case 'this_month':
        let this_month = moment().daysInMonth() - moment().date();
        query.start_date = moment().format('YYYY/MM/DD');
        query.finish_date = moment().add(this_month + 1, 'day').format('YYYY/MM/DD');
        break;
      case 'next_month':
        let next_month = moment().add((moment().daysInMonth() - moment().date()) + 1, 'day');
        query.start_date = next_month.format('YYYY/MM/DD');
        query.finish_date = next_month.add(next_month.daysInMonth(), 'day').format('YYYY/MM/DD');
        break;
      default:
        break;
    }
    return query;
  }

  // This method is responsible for obtaining the types of events
  loadtypeEvents() {
    this.businessService.getTypeEvent().subscribe((res: any) => {
      this.typesEvent = res.data;
    }, err => {
      this.uiService.showError(err, 'Error al cargar los tipos de vicinys');
    });
  }

  // This method is responsible for obtaining all popular events
  loadEventsPopulars() {
    this.businessService.getEventsPopular().subscribe((res: any) => {
      this.eventsPopular = res.data;
    }, err => {
      this.uiService.showError(err, 'Error al cargar los vicinys populares');
    });
  }

  // This method is responsible for loading the calendar
  selectDatetime() {
    $('.datetimepicker').datetimepicker({
      locale: 'es_ES',
      icons: {
        time: 'far fa-clock',
        date: 'fa fa-calendar',
        up: 'fa fa-chevron-up',
        down: 'fa fa-chevron-down',
        previous: 'fa fa-chevron-left',
        next: 'fa fa-chevron-right',
        today: 'fa fa-screenshot',
        clear: 'fa fa-trash',
        close: 'fa fa-remove'
      },
      format: 'Y/MM/DD',
    });
  }
}
