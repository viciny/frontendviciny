import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import * as Mapboxgl from 'mapbox-gl';
import * as MapboxGeocoder from '@mapbox/mapbox-gl-geocoder';

// =========================================
// SERVICES
// =========================================
import {
  BusinessService,
  UiService
} from './../../services/services.index';

// =========================================
// UTILITIES
// =========================================
declare var $: any;
declare function init_all();

// =========================================
// URL - APIKEYMAP
// =========================================
import { environment } from '../../../environments/environment';

@Component({
  selector: 'app-new-event',
  templateUrl: './new-event.component.html',
  styleUrls: ['./new-event.component.css']
})
export class NewEventComponent implements OnInit {

  apiKeyMap = environment.apiKeyMap;
  form: FormGroup;
  typeEvents: Array<any> = [];
  deparments: Array<any> = [];
  municipalities: Array<any> = [];
  file: File = null;
  imageTemp: string | ArrayBuffer = '/assets/images/stage.svg';
  imageText: any = 'Selecciona la imagen';

  map: Mapboxgl.Map;
  geocoder: MapboxGeocoder;
  marker: Mapboxgl.Marker = null;

  constructor(
    private bussinesService: BusinessService,
    private uiService: UiService,
    private router: Router
  ) {
    init_all();
    this.loadData();
  }

  ngOnInit() {
    init_all();
    this.selectDatetime();
    this.loadTypeEvent();
    this.loadDepartments();
    this.loadMap();
  }

  // This method is responsible to load map
  loadMap() {
    Mapboxgl.accessToken = this.apiKeyMap;
    this.map = new Mapboxgl.Map({
      container: 'mapevent', // container id
      style: 'mapbox://styles/mapbox/streets-v11',
      center: [-74.08174, 4.60971], // [LNG, LAT]
      zoom: 13
    });
    this.marker = new Mapboxgl.Marker({
      draggable: true
    })
    .setLngLat([-74.08174, 4.60971])
    .addTo(this.map);

    this.map.addControl(new Mapboxgl.NavigationControl());
  }

  // this method is responsible to sarch coordenates
  setCoordinates(){
    let department = $('#department option:selected').text()!="Seleccione el departamento..."?$('#department option:selected').text():'';
    let municipality = $('#municipality option:selected').text()!="Seleccione el minucipio..."?$('#municipality option:selected').text():'';
    let query = "colombia "+department+" "+municipality+" "+$('#address').val();
    this.bussinesService.getCoordinatesAddress(query).subscribe(
      (res:any) =>{
        if(res.features.length>0){
          this.map.setCenter(res.features[0].center)
          this.marker.setLngLat(res.features[0].center);
        }
      }
    );
  }

  // This method is responsible for calling the service to create an event.
  create() {
    this.form.patchValue({
        start_date: $('#start_date').val().replace(' am', '').replace(' pm', ''),
        finish_date: $('#finish_date').val().replace(' am', '').replace(' pm', '')
    });
    this.uiService.loading();
    const coordinates = this.marker.getLngLat();
    this.form.value['longitude'] = coordinates.lng;
    this.form.value['latitude'] = coordinates.lat;
    this.bussinesService.createdEvent(this.form.value).subscribe((res: any) => {
      if (this.file != null) {
        this.onUpload(res.data.id);
      } else {
        this.uiService.showSuccess('Evento creado con éxito!');
        this.router.navigate(['/mis-vicinys']);
      }
    }, err => {
      this.uiService.showError(err, 'Error al crear el evento!');
    });
  }

  // This method is responsible for loading the selected image
  onFileSelected(event) {
    this.file = <File>event.target.files[0];
    const reader = new FileReader();
    const urlImageTemp = reader.readAsDataURL(<File>event.target.files[0]);
    reader.onloadend = () => {
      this.imageTemp = reader.result;
    };
    this.imageText = <File>event.target.files[0].name;
  }

  // This method is responsible for uploading the selected image
  onUpload(eventId) {
    const fd = new FormData();
    fd.append('image', this.file);
    this.bussinesService.addImageEvent(eventId, fd).subscribe(res => {
      this.uiService.closeLoading();
      this.uiService.showSuccess('Evento creado con éxito!');
      this.router.navigate(['/mis-vicinys']);
    }, err => {
      this.uiService.showError(err, 'Error al cargar la imagen');
    }
    );
  }

  // This method is responsible for obtaining the types of events
  loadTypeEvent() {
    this.bussinesService.getTypeEvent().subscribe((res: any) => {
      this.typeEvents = res.data;
    }, err => {
      this.uiService.showError(err, 'Error al cargar los tipos de vicinys');
    });
  }

  // This method is responsible for obtaining the departments
  loadDepartments() {
    this.bussinesService.getDepartments().subscribe((res: any) => {
      this.deparments = res;
      if (this.deparments.length > 0) {
        this.loadMunicipalities(this.deparments[0].id);
      }
    }, err => {
      this.uiService.showError(err, 'Error al cargar los departamentos');
    });
  }

  // This method is responsible for obtaining the municipalities
  loadMunicipalities(departmentId) {
    this.bussinesService.getMinicipalities(departmentId).subscribe((res: any) => {
      this.municipalities = res;
      this.setCoordinates();
    }, err => {
      this.uiService.showError(err, 'Error al cargar los municipios');
    });
  }

  // This method is responsible for loading the data for the creation of an event.
  loadData() {
    this.form = new FormGroup({
      name: new FormControl(null, [Validators.required]),
      organizer: new FormControl(null, [Validators.required]),
      address: new FormControl(null, [Validators.required]),
      start_date: new FormControl(null, [
        Validators.pattern('[0-9]{4}/[0-9]{1,2}/[0-9]{1,2} [0-9]{1,2}:[0-9]{2}'),
        Validators.maxLength(16)
      ]),
      tickets: new FormControl(null, [Validators.required]),
      ticket_value: new FormControl(null, [Validators.required]),
      finish_date: new FormControl(null, [
        Validators.pattern('[0-9]{4}/[0-9]{1,2}/[0-9]{1,2} [0-9]{1,2}:[0-9]{2}'),
        Validators.maxLength(16)
      ]),
      description: new FormControl(null, [Validators.required]),
      type_event_id: new FormControl(null, [Validators.required]),
      municipality_id: new FormControl(null, [Validators.required])
    });
    this.form.setValue({
      name: '',
      organizer: '',
      address: '',
      start_date: '',
      tickets: '',
      ticket_value: '',
      finish_date: '',
      description: '',
      type_event_id: '',
      municipality_id: '',
    });
  }

  // This method is responsible for loading the calendar
  selectDatetime() {
    $('.datetimepicker').datetimepicker({
      locale: 'es_ES',
      icons: {
        time: 'far fa-clock',
        date: 'fa fa-calendar',
        up: 'fa fa-chevron-up',
        down: 'fa fa-chevron-down',
        previous: 'fa fa-chevron-left',
        next: 'fa fa-chevron-right',
        today: 'fa fa-screenshot',
        clear: 'fa fa-trash',
        close: 'fa fa-remove'
      },
      format: 'Y/M/D HH:mm a',
    });
  }
}
