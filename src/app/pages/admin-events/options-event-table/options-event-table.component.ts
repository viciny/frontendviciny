import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-options-event-table',
  templateUrl: './options-event-table.component.html',
  styleUrls: ['./options-event-table.component.css']
})
export class OptionsEventTableComponent implements OnInit {

  @Input() rowData: any;
  @Output() delete = new EventEmitter();
  @Output() edit = new EventEmitter();

  constructor(
    private router: Router
  ) {

  }

  ngOnInit() {
  }

  // This method is responsible for list of tickets.
  listTickes(){
    this.router.navigate(['/lista-de-pedidos', this.rowData.id]);
  }

  // This method is responsible for deleting a event.
  deleteEvent(){
    this.delete.emit(this.rowData);
  }

  // This method is responsible for update a event.
  editEvent() {
    this.edit.emit(this.rowData);
  }

}
