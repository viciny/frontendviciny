import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

// =========================================
// SERVICES
// =========================================
import {
  AdminService,
  UiService
} from './../../services/services.index';

// =========================================
// COMPONENTS
// =========================================
import { OptionsEventTableComponent } from './options-event-table/options-event-table.component';

// =========================================
// UTILITIES
// =========================================
declare function init_all();

@Component({
  selector: 'app-admin-events',
  templateUrl: './admin-events.component.html',
  styleUrls: ['./admin-events.component.css']
})
export class AdminEventsComponent implements OnInit {

  event: any = null;

  settings = {
    columns: {
      id: {
        title: 'ID'
      },
      name: {
        title: 'Nombre'
      },
      start_date: {
        title: 'Fecha'
      },
      organizer: {
        title: 'Organizador'
      },
      to_delete:{
        title: 'Para eliminar'
      },
      action: {
        title: 'Acciones',
        type: 'custom',
        renderComponent: OptionsEventTableComponent,
        onComponentInitFunction: (instance) => {
          instance.delete.subscribe(row => {
            this.deleteEvent(row.id);
          });
          instance.edit.subscribe(row => {
            this.showEvent(row.id);
          });
        },
        filter: false,
      }
    },
    actions: {
      columnTitle: null,
      add: false,
      edit: false,
      delete: false
    },
  };

  data = [];

  constructor(
    private rutaActiva: ActivatedRoute,
    private adminService: AdminService,
    private uiService: UiService,
  ) {
    init_all();
  }

  ngOnInit() {
    init_all();
    this.adminService.getUserEvents(this.rutaActiva.snapshot.params.user_id)
    .subscribe(
      (events: any) => {
        this.data = events.map(
          (event:any)=>{
            event.to_delete = event.by_delete?'Si':'No';
            return event;
          }
        );
      }
    );
  }

  // This method is responsible for deleting a event.
  deleteEvent(eventId) {
    this.adminService.deleteEvent(eventId).subscribe(
      event => {
        this.data = this.data.filter((event: any) => {
          return event.id !== eventId;
        });
      },
      err => {
        this.uiService.showError(err, 'Error al eliminar el evento');
      }
    );
  }

  // This method is responsible for displaying a event.
  showEvent(eventId) {
    this.adminService.getEvent(eventId).subscribe(
      (res: any) => {
        this.event = res.data;
        this.event['department_id'] = this.event.municipality.department.id;
        this.event['municipality_id'] = this.event.municipality.id;
      }
    );
  }

  // This method is responsible for update a event.
  editEvent(updateEvent) {
    this.data = this.data.map(event => {
      if (event.id === updateEvent.id) {
        event = updateEvent;
      }
      return event;
    });
  }
}
