import { Component, OnInit, Input, Output, EventEmitter, OnChanges } from '@angular/core';

// =========================================
// SERVICES
// =========================================
import {
  AdminService,
  BusinessService,
  UiService
} from './../../../services/services.index';

// =========================================
// UTILITIES
// =========================================
declare var $: any;

@Component({
  selector: 'app-modal-edit-event',
  templateUrl: './modal-edit-event.component.html',
  styleUrls: ['./modal-edit-event.component.css']
})
export class ModalEditEventComponent implements OnInit, OnChanges {

  @Input() event;
  @Output() sendEvent = new EventEmitter();

  deparments: Array<any> = [];
  municipalities: Array<any> = [];

  constructor(
    private adminService: AdminService,
    private bussinesService: BusinessService,
    private uiService: UiService,
  ) {
    this.loadDepartments();
  }

  ngOnChanges() {
    this.loadDepartments();
  }

  ngOnInit() {
    this.selectDatetime();
  }

  // This method is responsible for obtaining the departments
  loadDepartments() {
    this.bussinesService.getDepartments().subscribe((res: any) => {
      this.deparments = res;
      if (this.deparments.length > 0) {
        this.loadMunicipalities(this.event ? this.event.department_id : this.deparments[0].id);
      }
    }, err => {
      this.uiService.showError(err, 'Error al cargar los departamentos');
    });
  }

  // This method is responsible for obtaining the municipalities
  loadMunicipalities(departmentId) {
    this.bussinesService.getMinicipalities(departmentId).subscribe((res: any) => {
      this.municipalities = res;
    }, err => {
      this.uiService.showError(err, 'Error al cargar los municipios');
    });
  }

  // This method is responsible for update a event.
  update() {
    this.adminService.updateEvent(this.event).subscribe(
      (res: any) => {
        this.sendEvent.emit(res.data);
      },
      err => {
        this.uiService.showError(err, 'Error al actualizar el usuario!');
      }
    );
  }

  // This method is responsible for loading the calendar
  selectDatetime() {
    $('.datetimepicker').datetimepicker({
      locale: 'es_ES',
      icons: {
        time: 'far fa-clock',
        date: 'fa fa-calendar',
        up: 'fa fa-chevron-up',
        down: 'fa fa-chevron-down',
        previous: 'fa fa-chevron-left',
        next: 'fa fa-chevron-right',
        today: 'fa fa-screenshot',
        clear: 'fa fa-trash',
        close: 'fa fa-remove'
      },
      format: 'Y/M/D HH:mm a',
    });
  }

}
