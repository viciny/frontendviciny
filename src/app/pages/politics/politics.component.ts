import { Component, OnInit } from '@angular/core';

// =========================================
// UTILITIES
// =========================================
declare function init_all();

@Component({
  selector: 'app-politics',
  templateUrl: './politics.component.html',
  styleUrls: ['./politics.component.css']
})
export class PoliticsComponent implements OnInit {

  constructor() {
    init_all();
  }

  ngOnInit() {
    init_all();
  }

}
