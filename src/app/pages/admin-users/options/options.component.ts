import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';
@Component({
  selector: 'app-options',
  templateUrl: './options.component.html',
  styleUrls: ['./options.component.css']
})
export class OptionsComponent implements OnInit {

  @Input() rowData: any;
  @Output() delete = new EventEmitter();
  @Output() edit = new EventEmitter();

  constructor(
    private router: Router
  ) {}

  ngOnInit() {
  }

  // This method is responsible for displaying the list of events.
  listEvents() {
    this.router.navigate(['/lista-de-vicinys', this.rowData.id]);
  }

  // This method is responsible for deleting a user.
  deleteUser() {
    this.delete.emit(this.rowData);
  }

  // This method is responsible for updating a user
  editUser() {
    this.edit.emit(this.rowData);
  }

}
