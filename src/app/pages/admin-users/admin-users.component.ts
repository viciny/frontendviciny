import { Component, OnInit } from '@angular/core';

// =========================================
// SERVICES
// =========================================
import {
  AdminService,
  UiService
} from './../../services/services.index';

// =========================================
// COMPONENTS
// =========================================
import { OptionsComponent } from './options/options.component';

// =========================================
// UTILITIES
// =========================================
declare function init_all();

@Component({
  selector: 'app-admin-users',
  templateUrl: './admin-users.component.html',
  styleUrls: ['./admin-users.component.css']
})
export class AdminUsersComponent implements OnInit {

  settings = {
    columns: {
      id: {
        title: 'ID'
      },
      name: {
        title: 'Nombre'
      },
      email: {
        title: 'Email'
      },
      action: {
        title: 'Acciones',
        type: 'custom',
        renderComponent: OptionsComponent,
        onComponentInitFunction: (instance) => {
          instance.delete.subscribe(row => {
            this.deleteUser(row.id);
          });
          instance.edit.subscribe(row => {
            this.showUser(row.id);
          });
        },
        filter: false,
      }
    },
    actions: {
      columnTitle: null,
      add: false,
      edit: false,
      delete: false
    },
  };

  data = [];
  user = null;

  constructor(
    private adminService: AdminService,
    private uiService: UiService,
  ) {
    init_all();
    this.adminService.getUsers().subscribe(
      (users: any) => {
        this.data = users.map(
          user => {
            return user;
          }
        );
      }
    );
  }

  ngOnInit() {
    init_all();
  }

  // This method is responsible for deleting a user.
  deleteUser(userId) {
    this.adminService.deleteUser(userId).subscribe(user => {
        this.data = this.data.filter((user: any) => {
          return user.id !== userId;
        });
      },
      err => {
        this.uiService.showError(err, 'Error al eliminar el usuario');
      }
    );
  }

  // This method is responsible for displaying a user.
  showUser(userId) {
    this.adminService.getUser(userId).subscribe(
      (user: any) => {
        this.user = user;
        this.user.user_id = user.id;
        this.user.type_document = '';
        this.user.municipality_id = '';
        this.user.department_id = '';
        if (this.user.profile) {
          this.user.type_document = user.profile.type_document;
          this.user.nit = user.profile.nit;
          this.user.phone = user.profile.phone;
          this.user.account_bank = user.profile.account_bank;
          this.user.address = user.profile.address;
          this.user.bank_name = user.profile.bank_name;
          this.user.municipality_id = user.profile.municipality.id;
          this.user.department_id = user.profile.municipality.department_id;
        }
      }
    );
  }

  // This method is responsible for updating a user.
  editUser(updateUser) {
    this.data = this.data.map(user => {
      if (user.id === updateUser.id) {
        user = updateUser;
      }
      return user;
    });
  }
}
