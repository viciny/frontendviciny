import { Component, OnInit, Input, Output, EventEmitter, OnChanges } from '@angular/core';

// =========================================
// SERVICES
// =========================================
import {
  AdminService,
  BusinessService,
  UiService
} from './../../../services/services.index';

@Component({
  selector: 'app-modal-edit-user',
  templateUrl: './modal-edit-user.component.html',
  styleUrls: ['./modal-edit-user.component.css']
})
export class ModalEditUserComponent implements OnInit, OnChanges {

  @Input() user;
  @Output() sendUser = new EventEmitter();

  deparments: Array<any> = [];
  municipalities: Array<any> = [];

  constructor(
    private adminService: AdminService,
    private bussinesService: BusinessService,
    private uiService: UiService,
  ) {
    this.loadDepartments();
   }

  ngOnInit() { }

  ngOnChanges() {
    this.loadDepartments();
  }

  // This method is responsible for obtaining the departments
  loadDepartments() {
    this.bussinesService.getDepartments().subscribe((res: any) => {
      this.deparments = res;
      if (this.deparments.length > 0) {
        this.loadMunicipalities(this.user && this.user.department_id !== '' ? this.user.department_id : this.deparments[0].id);
      }
    }, err => {
      this.uiService.showError(err, 'Error al cargar los departamentos');
    });
  }

  // This method is responsible for obtaining the municipalities
  loadMunicipalities(departmentId) {
    this.bussinesService.getMinicipalities(departmentId).subscribe((res: any) => {
      this.municipalities = res;
    }, err => {
      this.uiService.showError(err, 'Error al cargar los municipios');
    });
  }

  // This method is responsible for updating a user.
  update() {
    this.adminService.updateUser(this.user).subscribe(
      (res: any) => {
        this.sendUser.emit(res.user);
      },
      err => {
        this.uiService.showError(err, 'Error al actualizar el usuario!');
      }
    );
  }

}
