import { Component, OnInit } from '@angular/core';

// =========================================
// SERVICES
// =========================================
import {
  BusinessService,
} from '../../services/services.index';

// =========================================
// UTILITIES
// =========================================
declare function init_all();

@Component({
  selector: 'app-show-scanner',
  templateUrl: './show-scanner.component.html',
  styleUrls: ['./show-scanner.component.css']
})
export class ShowScannerComponent implements OnInit {

  event: any;
  user: any;
  state = false;
  scanner = true;

  constructor(
    private businessService: BusinessService
  ) {
    init_all();
  }

  ngOnInit() {
    init_all();
  }

  scanSuccessHandler($event){
    this.businessService.checkTicket($event).subscribe((res: any) => {
        this.scanner = false;
        this.event = res.event;
        this.user = res.user;
        this.state = res.state;
      },
      error => {
        this.event = null;
        this.user = null;
        this.state = false;
      }
    );
  }

  newScannear() {
    if (!this.scanner) {
      this.scanner = true;
      this.event = null;
      this.user = null;
      this.state = false;
    }
  }
}
