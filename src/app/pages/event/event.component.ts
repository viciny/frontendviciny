import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Title } from '@angular/platform-browser';
import * as Mapboxgl from 'mapbox-gl';

// =========================================
// MODELS - EVENT
// =========================================
import { Event } from '../../models/event';

// =========================================
// SERVICES
// =========================================
import {
  BusinessService,
  UiService,
  AuthService
} from '../../services/services.index';

// =========================================
// URL - APIKEYMAP
// =========================================
import { environment } from '../../../environments/environment';

// =========================================
// UTILITIES
// =========================================
declare var ePayco: any;
declare function init_all();

@Component({
  selector: 'app-event',
  templateUrl: './event.component.html',
  styleUrls: ['./event.component.css']
})
export class EventComponent implements OnInit {

  event: Event = null;
  apiKeyMap = environment.apiKeyMap;
  eventModal: Event;
  map: Mapboxgl.Map;
  markerMap: Mapboxgl.Marker;

  constructor(
    private authService: AuthService,
    private router: Router,
    private bussinesService: BusinessService,
    private routeActive: ActivatedRoute,
    private uiService: UiService,
    private titleRoute: Title
  ) {
    init_all();
    this.loadEvent(this.routeActive.snapshot.params.event_id);
  }

  ngOnInit() {
    init_all();
    this.loadMap();
  }

  // This method is responsible to load map
  loadMap() {
    Mapboxgl.accessToken = this.apiKeyMap;
    this.map = new Mapboxgl.Map({
      container: 'mapevent', // container id
      style: 'mapbox://styles/mapbox/streets-v11',
      center: [-74.08174, 4.60971], // [LNG, LAT]
      zoom: 12
    });
    this.markerMap = new Mapboxgl.Marker();
    this.markerMap.setLngLat([-74.08174, 4.60971]).addTo(this.map);
  }

  // This method is responsible for obtaining the selected event
  loadEvent(eventId) {
    this.uiService.loading();
    this.bussinesService.getEvent(eventId).subscribe((res: any) => {
      this.event = res.data;
      this.titleRoute.setTitle('Viciny - ' + this.event.name);
      this.map.setCenter([res.data.longitude, res.data.latitude]);
      this.markerMap.setLngLat([res.data.longitude, res.data.latitude]);
      this.uiService.closeLoading();
    }, err => {
      this.uiService.showError(err, 'Error al cargar viciny');
    });
  }

  // This method allows you to add an event to favorites
  addEventFavorite(event_id) {
    if (this.authService.isLogin()) {
      this.bussinesService.addEventFavorite({ event_id }).subscribe(res => {
        this.uiService.showSuccess('Viciny agregado a favoritos!');
      }, (err: any) => {
        if (err.status == 422) {
          this.uiService.showSuccess('Viciny ya esta en la lista de favoritos!');
        } else {
          this.uiService.showError(err, 'Error al agregar a favoritos');
        }
      });
    } else {
      this.router.navigate(['/login']);
    }
  }


}
