import { Component, OnInit } from '@angular/core';
import Swal from 'sweetalert2';

// =========================================
// SERVICES
// =========================================
import {
  BusinessService,
  UiService
} from '../../services/services.index';

// =========================================
// MODEL - EVENT
// =========================================
import { Event } from '../../models/event';

// =========================================
// UTILITIES
// =========================================
declare var $: any;
declare function init_all();

@Component({
  selector: 'app-favorites',
  templateUrl: './favorites.component.html',
  styleUrls: ['./favorites.component.css']
})
export class FavoritesComponent implements OnInit {

  events: Array<Event> = [];
  selectedEvent: Event = null;

  constructor(
    private businessService: BusinessService,
    private uiService: UiService
  ) {
    init_all();
    this.loadEventsFavorites();
  }

  ngOnInit() {
    init_all();
  }

  // This method is responsible for obtaining the favorite events
  loadEventsFavorites() {
    this.uiService.loading();
    this.businessService.getEventsFavorites().subscribe((res: any) => {
      this.events = res.data;
      this.uiService.closeLoading();
    }, err => {
      this.uiService.showError(err, 'Error al obtener los vicinys favoritos');
    });
  }

  // This method allows you to remove an event to favorites
  deleteFavoriteEvent(eventId) {
    this.businessService.deleteFavoriteEvent(eventId).subscribe(res => {
      this.events = this.events.filter( item => {
        return item.id !== eventId;
      });
    }, err => {
      this.uiService.showError(err, 'Error al eliminar viciny de favoritos');
    });
  }

  // This method is responsible for show alert to delete a event favorite
  deletFavoriteAlert(eventId) {
    Swal.fire({
      title: '¿Estás seguro?',
      text: 'No podrás revertir esto!',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#5724A1',
      cancelButtonColor: '#f44336',
      confirmButtonText: 'Si, eliminar!',
      cancelButtonText: 'No, mantenerlo!'
    }).then((result) => {
      if (result.value) {
        this.deleteFavoriteEvent(eventId);
        Swal.fire(
          'Viciny eliminado!',
          'Tu viciny ha sido eliminado de favoritos correctamente.',
          'success'
        );
      }
    });
  }

  // This method is responsible for opening the modality of buying tickets for a selected event
  selectEvent(event) {
    this.selectedEvent = event;
    $('#buyticket').modal('show');
  }

}
