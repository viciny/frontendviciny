import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

// =========================================
// UTILITIES
// =========================================
declare function init_all();

@Component({
  selector: 'app-ticket-success',
  templateUrl: './ticket-success.component.html',
  styleUrls: ['./ticket-success.component.css']
})
export class TicketSuccessComponent implements OnInit {

  type: string;

  constructor(
    private activatedRoute: ActivatedRoute
  ) {
    init_all();
    this.activatedRoute.params.subscribe(params => {
      this.type = params.id;
    });
  }

  ngOnInit() {
    init_all();
  }

}
