// =========================================
// MODULES
// =========================================
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '../shared/shared.module';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { ZXingScannerModule } from '@zxing/ngx-scanner';

// =========================================
// ROUTING
// =========================================
import { PAGES_ROUTES } from './pages.routes';

// =========================================
// COMPONENTS
// =========================================
import { ForYouComponent } from './../components/for-you/for-you.component';
import { SocialFeedComponent } from '../components/social-feed/social-feed.component';
import { ShowEventsComponent } from './../components/show-events/show-events.component';
import { ModalPayComponent } from './../components/modal-pay/modal-pay.component';
import { ShareSocialComponent } from './../components/share-social/share-social.component';

// =========================================
// PAGES
// =========================================
import { PagesComponent } from './pages.component';
import { WelcomeComponent } from './welcome/welcome.component';
import { EventComponent } from './event/event.component';
import { KeysComponent } from './keys/keys.component';
import { FavoritesComponent } from './favorites/favorites.component';
import { NewEventComponent } from './new-event/new-event.component';
import { VicinysComponent } from './vicinys/vicinys.component';
import { ProfileComponent } from './profile/profile.component';
import { TermsComponent } from './terms/terms.component';
import { PoliticsComponent } from './politics/politics.component';
import { EventsSearchComponent } from './events-search/events-search.component';
import { AdminEventsComponent } from './admin-events/admin-events.component';
import { AdminUsersComponent } from './admin-users/admin-users.component';
import { OptionsComponent } from './admin-users/options/options.component';
import { OptionsEventTableComponent } from './admin-events/options-event-table/options-event-table.component';
import { AdminTickesComponent } from './admin-tickes/admin-tickes.component';
import { OptionsTickeTableComponent } from './admin-tickes/options-ticke-table/options-ticke-table.component';
import { ModalEditUserComponent } from './admin-users/modal-edit-user/modal-edit-user.component';
import { ModalEditEventComponent } from './admin-events/modal-edit-event/modal-edit-event.component';
import { ModalEditPersonalEventComponent } from './vicinys/modal-edit-personal-event/modal-edit-personal-event.component';
import { TicketSuccessComponent } from './ticket-success/ticket-success.component';
import { ShowScannerComponent } from './show-scanner/show-scanner.component';
import { VerifyEmailComponent } from './verify-email/verify-email.component';
import { AdminTicketsOrderComponent } from './admin-tickets-order/admin-tickets-order.component';
import { OptionTicketsOrderTableComponent } from './admin-tickets-order/option-tickets-order-table/option-tickets-order-table.component';

@NgModule({
  declarations: [
    PagesComponent,
    WelcomeComponent,
    EventComponent,
    KeysComponent,
    FavoritesComponent,
    NewEventComponent,
    VicinysComponent,
    ForYouComponent,
    ProfileComponent,
    TermsComponent,
    PoliticsComponent,
    SocialFeedComponent,
    EventsSearchComponent,
    ShowEventsComponent,
    AdminEventsComponent,
    AdminUsersComponent,
    AdminTickesComponent,
    OptionsComponent,
    OptionsEventTableComponent,
    OptionsTickeTableComponent,
    ModalEditUserComponent,
    ModalEditEventComponent,
    ModalEditPersonalEventComponent,
    ModalPayComponent,
    TicketSuccessComponent,
    ShowScannerComponent,
    VerifyEmailComponent,
    ShareSocialComponent,
    AdminTicketsOrderComponent,
    OptionTicketsOrderTableComponent
  ],
  entryComponents: [OptionsComponent, OptionsEventTableComponent, OptionsTickeTableComponent, OptionTicketsOrderTableComponent],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
    Ng2SmartTableModule,
    ZXingScannerModule,
    PAGES_ROUTES
  ],
  exports: [
    PagesComponent,
    WelcomeComponent,
    EventComponent,
    KeysComponent,
    FavoritesComponent,
    NewEventComponent,
    VicinysComponent,
    ForYouComponent,
    ProfileComponent,
    TermsComponent,
    PoliticsComponent,
    SocialFeedComponent,
    EventsSearchComponent,
    ShowEventsComponent,
    AdminEventsComponent,
    AdminUsersComponent,
    AdminTickesComponent,
    OptionsComponent,
    OptionsEventTableComponent,
    OptionsTickeTableComponent,
    ModalEditUserComponent,
    ModalEditEventComponent,
    ModalEditPersonalEventComponent,
    TicketSuccessComponent,
    ShowScannerComponent,
    VerifyEmailComponent,
    ShareSocialComponent,
    AdminTicketsOrderComponent,
    OptionTicketsOrderTableComponent
  ]
})
export class PagesModule { }
