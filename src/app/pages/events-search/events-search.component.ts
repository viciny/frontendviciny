import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

// =========================================
// SERVICES
// =========================================
import {
  BusinessService,
  UiService,
  AuthService
} from '../../services/services.index';

// =========================================
// UTILITIES
// =========================================
declare var $: any;
declare var moment: any;
declare function init_all();

@Component({
  selector: 'app-events-search',
  templateUrl: './events-search.component.html',
  styleUrls: ['./events-search.component.css']
})
export class EventsSearchComponent implements OnInit {

  typesEvent: Array<any> = [];
  events: Array<Event> = [];
  selectTypeEvent = 'todos';
  meta: any;
  date = 'any_date';
  municipality = '';
  name = '';
  organizer = '';
  eventModal: Event;

  constructor(
    private businessService: BusinessService,
    private authService: AuthService,
    private uiService: UiService,
    private router: Router
  ) {
    init_all();
    this.loadtypeEvents();
  }

  ngOnInit() {
    init_all();
    this.selectDatetime();
    this.searchEvent();
  }

  // This method allows load to event selected
  openShared(event: Event) {
    this.eventModal = event;
  }

  // This method is responsible for obtaining the types of events
  loadtypeEvents() {
    this.businessService.getTypeEvent().subscribe((res: any) => {
      this.typesEvent = res.data;
    }, err => {
      this.uiService.showError(err, 'Error al cargar los tipos de vicinys');
    });
  }

  // This method is responsible for obtaining the current day
  getToday() {
    const f = new Date();
    return f.toISOString().split('T')[0];
  }

  // This method is responsible for loading the calendar
  selectDatetime() {
    $('.datetimepicker').datetimepicker({
      locale: 'es_ES',
      icons: {
        time: 'far fa-clock',
        date: 'fa fa-calendar',
        up: 'fa fa-chevron-up',
        down: 'fa fa-chevron-down',
        previous: 'fa fa-chevron-left',
        next: 'fa fa-chevron-right',
        today: 'fa fa-screenshot',
        clear: 'fa fa-trash',
        close: 'fa fa-remove'
      },
      format: 'Y/MM/DD',
    });
  }

  // This method is responsible for conducting an event search
  searchEvent(page = '') {
    const query = {};
    const dateQuery = this.transformDateQuery();
    if (dateQuery.start_date !== null) {
      query['start_date'] = dateQuery.start_date;
    }
    if (dateQuery.finish_date !== null) {
      query['finish_date'] = dateQuery.finish_date;
    }
    if (this.municipality !== '') {
      query['municipality'] = this.municipality;
    }
    if (this.selectTypeEvent !== 'todos') {
      query['type_event_id'] = this.selectTypeEvent;
    }
    if (this.name !== '') {
      query['name'] = this.name;
    }
    if (this.organizer !== '') {
      query['organizer'] = this.organizer;
    }
    this.uiService.loading();
    this.businessService.searchEvents(query, page).subscribe(
      (res: any) => {
        this.events = res.data;
        this.meta = res.meta;
        this.uiService.closeLoading();
      }, err => {
        this.uiService.showError(err, 'Error en la busqueda de vicinys');
      }
    );
  }

  // This method is responsible for transforming the date
  transformDateQuery() {
    const query = {
      start_date: null,
      finish_date: null
    };
    switch (this.date) {
      case 'today':
        query.start_date = moment().format('YYYY/MM/DD');
        query.finish_date = moment().add(1, 'day').format('YYYY/MM/DD');
        break;
      case 'tomorrow':
        query.start_date = moment().add(1, 'day').format('YYYY/MM/DD');
        query.finish_date = moment().add(2, 'day');
        break;
      case 'this_weekend':
        let this_weekend = 7 - moment().day();
        query.start_date = moment().add(this_weekend - 2 >= 0 ? this_weekend - 2 : 0, 'day').format('YYYY/MM/DD');
        query.finish_date = moment().add(this_weekend, 'day').format('YYYY/MM/DD');
        break;
      case 'this_week':
        let this_week = 7 - moment().day();
        query.start_date = moment().format('YYYY/MM/DD');
        query.finish_date = moment().add(this_week + 1, 'day').format('YYYY/MM/DD');
        break;
      case 'next_week':
        let next_week = 7 - moment().day();
        query.start_date = moment().add(next_week + 1, 'day').format('YYYY/MM/DD');
        query.finish_date = moment().add(next_week + 8, 'day').format('YYYY/MM/DD');
        break;
      case 'this_month':
        let this_month = moment().daysInMonth() - moment().date();
        query.start_date = moment().format('YYYY/MM/DD');
        query.finish_date = moment().add(this_month + 1, 'day').format('YYYY/MM/DD');
        break;
      case 'next_month':
        let next_month = moment().add((moment().daysInMonth() - moment().date()) + 1, 'day');
        query.start_date = next_month.format('YYYY/MM/DD');
        query.finish_date = next_month.add(next_month.daysInMonth(), 'day').format('YYYY/MM/DD');
        break;
      default:
        break;
    }
    return query;
  }

  // This method allows you to add an event to favorites
  addEventFavorite(event_id) {
    if (this.authService.isLogin()) {
      this.businessService.addEventFavorite({ event_id }).subscribe(res => {
        this.uiService.showSuccess('Viciny agregado a favoritos!');
      }, (err: any) => {
        if (err.status == 422) {
          this.uiService.showSuccess('Viciny ya esta en la lista de favoritos!');
        } else {
          this.uiService.showError(err, 'Error al agregar a favoritos');
        }
      });
    } else {
      this.router.navigate(['/login']);
    }
  }
}
