import { Component, OnInit } from '@angular/core';

// =========================================
// UTILITIES
// =========================================
declare function init_all();

@Component({
  selector: 'app-verify-email',
  templateUrl: './verify-email.component.html',
  styleUrls: ['./verify-email.component.css']
})
export class VerifyEmailComponent implements OnInit {

  constructor() {
    init_all();
  }

  ngOnInit() {
    init_all();
  }

}
