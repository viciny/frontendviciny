import { Component, OnInit } from '@angular/core';

// =========================================
// UTILITIES
// =========================================
declare function init_all();

@Component({
  selector: 'app-terms',
  templateUrl: './terms.component.html',
  styleUrls: ['./terms.component.css']
})
export class TermsComponent implements OnInit {

  constructor() {
    init_all();
  }

  ngOnInit() {
    init_all();
  }

}
