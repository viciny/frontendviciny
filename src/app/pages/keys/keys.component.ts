import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

// =========================================
// SERVICES
// =========================================
import {
  BusinessService,
  UiService
} from '../../services/services.index';

// =========================================
// UTILITIES
// =========================================
declare function init_all();

@Component({
  selector: 'app-keys',
  templateUrl: './keys.component.html',
  styleUrls: ['./keys.component.css']
})
export class KeysComponent implements OnInit {

  tickets: any[] = [];
  constructor(
    private bussinesService: BusinessService,
    private uiService: UiService,
    private router: Router
  ) {
    init_all();
    this.loadKeys();
   }

  ngOnInit() {
    init_all();
  }

  // This method is responsible for obtaining the keys
  loadKeys() {
    this.uiService.loading();
    this.bussinesService.getTickets().subscribe((res: any) => {
        this.tickets = res.data;
        this.uiService.closeLoading();
    }, err => {
        this.uiService.showError(err, 'Error al obtener las llaves');
    });
  }
}
