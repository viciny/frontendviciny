// =========================================
// ROUTING
// =========================================
import { Routes, RouterModule } from '@angular/router';

// =========================================
// GUARDS
// =========================================
import { AuthGuard } from './../guards/auth.guard';

// =========================================
// PAGES
// =========================================
import { PagesComponent } from './pages.component';
import { WelcomeComponent } from './welcome/welcome.component';
import { EventComponent } from './event/event.component';
import { KeysComponent } from './keys/keys.component';
import { FavoritesComponent } from './favorites/favorites.component';
import { NewEventComponent } from './new-event/new-event.component';
import { VicinysComponent } from './vicinys/vicinys.component';
import { ProfileComponent } from './profile/profile.component';
import { PoliticsComponent } from './politics/politics.component';
import { TermsComponent } from './terms/terms.component';
import { EventsSearchComponent } from './events-search/events-search.component';
import { AdminUsersComponent } from './admin-users/admin-users.component';
import { AdminEventsComponent } from './admin-events/admin-events.component';
import { AdminTickesComponent } from './admin-tickes/admin-tickes.component';
import { TicketSuccessComponent } from './ticket-success/ticket-success.component';
import { ShowScannerComponent } from './show-scanner/show-scanner.component';
import { VerifyEmailComponent } from './verify-email/verify-email.component';
import { AdminTicketsOrderComponent } from './admin-tickets-order/admin-tickets-order.component';

const pagesRoutes: Routes = [
  {
    path: '',
    component: PagesComponent,
    children: [
      { path: 'inicio', component: WelcomeComponent, data: { title: 'Bienvenido', imgLogo: 'name.svg' } },
      { path: 'busqueda', component: EventsSearchComponent, data: { title: 'Busqueda', imgLogo: 'name-purple.svg' } },
      { path: 'viciny/:event_id', component: EventComponent, data: { title: 'Viciny', imgLogo: 'name-purple.svg' } },
      {
        path: 'estado-de-la-compra/:id',
        component: TicketSuccessComponent,
        canActivate: [ AuthGuard ],
        data: { title: 'Estado de compra', imgLogo: 'name-purple.svg' } },
      { path: 'scannear-ticket', component: ShowScannerComponent, data: { title: 'Scannear ticket', imgLogo: 'name-purple.svg',canActivate: [ AuthGuard ] } },
      { path: 'email-verificado', component: VerifyEmailComponent, data: { title: 'Email verificado', imgLogo: 'name-purple.svg' } },
      {
        path: 'mis-llaves', component: KeysComponent, data: { title: 'Mis llaves', imgLogo: 'name-purple.svg' },
        canActivate: [ AuthGuard ]
      },
      {
        path: 'mis-favoritos', component: FavoritesComponent, data: { title: 'Mis favoritos', imgLogo: 'name-purple.svg' },
        canActivate: [ AuthGuard ]
      },
      {
        path: 'mis-vicinys', component: VicinysComponent, data: { title: 'Mis vicinys', imgLogo: 'name-purple.svg' },
        canActivate: [ AuthGuard ]
      },
      {
        path: 'crear-viciny', component: NewEventComponent, data: { title: 'Crear viciny', imgLogo: 'name-purple.svg' },
        canActivate: [ AuthGuard ]
      },
      {
        path: 'perfil-de-usuario', component: ProfileComponent, data: { title: 'Perfil de usuario', imgLogo: 'name-purple.svg' },
        canActivate: [ AuthGuard ]
      },
      {
        path: 'politicas-de-privacidad', component: PoliticsComponent,
        data: { title: 'Politicas de privacidad', imgLogo: 'name-purple.svg' }
      },
      {
        path: 'terminos-y-condiciones',
        component: TermsComponent,
        data: { title: 'Terminos de condiciones y uso', imgLogo: 'name-purple.svg' }
      },
      {
        path: 'lista-de-usuarios',
        component: AdminUsersComponent,
        data: { title: 'Lista de usuarios', imgLogo: 'name-purple.svg' },
        canActivate: [ AuthGuard ]
      },
      {
        path: 'lista-de-vicinys/:user_id',
        component: AdminEventsComponent,
        data: { title: 'Lista de vicinys', imgLogo: 'name-purple.svg' },
        canActivate: [ AuthGuard ]
      },
      {
        path: 'lista-de-pedidos/:event_id',
        component: AdminTicketsOrderComponent ,
        data: { title: 'Lista de tickets', imgLogo: 'name-purple.svg' },
        canActivate: [ AuthGuard ]
      },
      {
        path: 'lista-de-tickets/:tickets_order_id',
        component: AdminTickesComponent,
        data: { title: 'Lista de tickets', imgLogo: 'name-purple.svg' },
        canActivate: [ AuthGuard ]
      },
      { path: '', redirectTo: '/inicio', pathMatch: 'full' },
    ]
  }
];

export const PAGES_ROUTES = RouterModule.forChild(pagesRoutes);
