import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

// =========================================
// SERVICES
// =========================================
import {
  AdminService,
  UiService
} from './../../services/services.index';

// =========================================
// COMPONENTS
// =========================================
import { OptionTicketsOrderTableComponent } from './option-tickets-order-table/option-tickets-order-table.component';

// =========================================
// UTILITIES
// =========================================
declare function init_all();

@Component({
  selector: 'app-admin-tickets-order',
  templateUrl: './admin-tickets-order.component.html',
  styleUrls: ['./admin-tickets-order.component.css']
})
export class AdminTicketsOrderComponent implements OnInit {


  event: any = null;

  settings = {
    columns: {
      name: {
        title: 'Nombre'
      },
      email: {
        title: 'Email'
      },
      ref_payco:{
        title:"Ref payco"
      },
      state: {
        title: 'Estado'
      },
      tickets: {
        title: 'Numero de llaves'
      },
      action: {
        title: 'Acciones',
        type: 'custom',
        renderComponent: OptionTicketsOrderTableComponent,
        onComponentInitFunction: (instance) => {
          instance.delete.subscribe(row => {
            this.deleteEvent(row.id);
          });
        },
        filter: false,
      }
    },
    actions: {
      columnTitle: null,
      add: false,
      edit: false,
      delete: false
    },
  };

  data = [];

  constructor(
    private rutaActiva: ActivatedRoute,
    private adminService: AdminService,
    private uiService: UiService,
  ) {
    init_all();
  }

  ngOnInit() {
    init_all();
    this.adminService.getTicketsOrders(this.rutaActiva.snapshot.params.event_id)
    .subscribe((ticketsOrders: any) => {
        this.data = ticketsOrders.data.map((tickets_order: any) => {
          tickets_order.name = tickets_order.user.name;
          tickets_order.email = tickets_order.user.email;
          tickets_order.state = tickets_order.paid === 1 ? 'Pagado' : 'En proceso';
          return tickets_order;
        });
      }
    );
  }

  // This method is responsible for deleting a event.
  deleteEvent(orderId) {
    this.adminService.deleteTicketsOrders(orderId).subscribe(
      event => {
        this.data = this.data.filter((order: any) => {
          return order.id !== orderId;
        });
      },
      err => {
        this.uiService.showError(err, 'Error al eliminar el pedido de tickets');
      }
    );
  }

  // This method is responsible for displaying a event.
  showEvent(eventId) {
    this.adminService.getEvent(eventId).subscribe(
      (res: any) => {
        this.event = res.data;
        this.event['department_id'] = this.event.municipality.department.id;
        this.event['municipality_id'] = this.event.municipality.id;
      }
    );
  }
}
