import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-option-tickets-order-table',
  templateUrl: './option-tickets-order-table.component.html',
  styleUrls: ['./option-tickets-order-table.component.css']
})
export class OptionTicketsOrderTableComponent implements OnInit {

  @Input() rowData: any;
  @Output() delete = new EventEmitter();

  constructor(
    private router: Router
  ) {}

  ngOnInit() {
  }

  // // This method is responsible for displaying the list of tickets.
  listTickes() {
    console.log(this.rowData);
    this.router.navigate(['/lista-de-tickets', this.rowData.id]);
  }

  // This method is responsible for deleting a event.
  deleteEvent() {
    this.delete.emit(this.rowData);
  }

}
