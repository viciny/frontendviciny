// =========================================
// MODULES
// =========================================
import { NgModule } from '@angular/core';

// =========================================
// ROUTES
// =========================================
import { Routes, RouterModule } from '@angular/router';

// =========================================
// PAGES
// =========================================
import { RegisterComponent } from './auth/register/register.component';
import { LoginComponent } from './auth/login/login.component';

// =========================================
// GUARDS
// =========================================
import { NotauthGuard } from './guards/notauth.guard';

const routes: Routes = [
  { path: 'login', component: LoginComponent,
  canActivate: [ NotauthGuard ]},
  { path: 'registro', component: RegisterComponent,
  canActivate: [ NotauthGuard ] },
  //{ path: '**', component: NopagefoundComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
