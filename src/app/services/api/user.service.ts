import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { map } from 'rxjs/operators';
import { User } from '../../models/user';
@Injectable({
  providedIn: 'root'
})
export class UserService {

  private apiUrl = environment.apiUrl;
  
  user:User;
  constructor(private http: HttpClient) { 
    this.user  = JSON.parse(localStorage.getItem('user'));
  }

  getHeaders(){
    const headers = new HttpHeaders({
      'Authorization': `Bearer ${localStorage.getItem('token')}`,
      'Accept':'application/json'
    });

    return headers;
  }

  resendEmail(){
    return this.http.get(`${this.apiUrl}/profile/verify-email`,{headers:this.getHeaders()});
  }

  getUser() {
    return this.http.get(`${this.apiUrl}/profile/user`, { headers: this.getHeaders() })
    .toPromise().then(
      (data:any)=>{
        this.refresUser(data.user);
        return this.user;
      }
    );
  }

  updateProfile(datos:any){
    return this.http.put(`${this.apiUrl}/profile/update`,datos,
    { headers: this.getHeaders() })
    .pipe(map(
      (data:any)=>{
        this.refresUser(data.user);
        return data;
      }
    ));
  }

  addPhoto(photo){
    return this.http.post(`${this.apiUrl}/profile/photo`,photo,
    { headers: this.getHeaders() })
    .pipe(map(
      (data:any)=>{
        this.user.profile = data;
        this.refresUser(this.user);
        return data;
      }
    ));
  }

  refresUser(user:User){
    localStorage.setItem('user', JSON.stringify(user));
    this.user =user;
  }
}
