import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AdminService {

  private apiUrl = environment.apiUrl;
  constructor(private http: HttpClient) {

  }

  getHeaders(){
    const headers = new HttpHeaders({
      'Authorization': `Bearer ${localStorage.getItem('token')}`,
      'Accept':'application/json'
    });

    return headers;
  }

  getUsers(){
    return this.http.get(`${this.apiUrl}/admin/users`,{ headers: this.getHeaders() });
  }

  updateUser(user){
    return this.http.put(`${this.apiUrl}/admin/update-user`,user,{ headers: this.getHeaders() });
  }

  updateEvent(event){
    return this.http.put(`${this.apiUrl}/admin/update-event`,event,{ headers: this.getHeaders() });
  }

  getUser(user_id){
    return this.http.get(`${this.apiUrl}/admin/show-user/${user_id}`,{ headers: this.getHeaders() });
  }

  deleteUser(user_id){
    return this.http.delete(`${this.apiUrl}/admin/delete-user/${user_id}`,{ headers: this.getHeaders() });
  }

  getUserEvents(user_id){
    return this.http.get(`${this.apiUrl}/admin/user-events/${user_id}`,{ headers: this.getHeaders() });
  }

  getEvent(event_id){
    return this.http.get(`${this.apiUrl}/event/${event_id}`,{ headers: this.getHeaders() });
  }

  getTicketsOrders(event_id){
    return this.http.get(`${this.apiUrl}/admin/tickets-orders/${event_id}`,{ headers: this.getHeaders() });
  }

  deleteTicketsOrders(tickets_order_id){
    return this.http.delete(`${this.apiUrl}/admin/tickets-orders/${tickets_order_id}`,{ headers: this.getHeaders() });
  }

  deleteEvent(event_id){
    return this.http.delete(`${this.apiUrl}/admin/delete-event/${event_id}`,{ headers: this.getHeaders() });
  }

  getTickets(tickets_order_id){
    return this.http.get(`${this.apiUrl}/admin/tickets/${tickets_order_id}`,{ headers: this.getHeaders()});
  }

  deleteTicket(ticket_id){
    return this.http.delete(`${this.apiUrl}/ticket/${ticket_id}`,{ headers: this.getHeaders() });
  }

}
