import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { map } from 'rxjs/operators';
@Injectable({
  providedIn: 'root'
})
export class BusinessService {

  private apiUrl = environment.apiUrl;
  constructor(private http: HttpClient) {

  }

  getHeaders(){
    const headers = new HttpHeaders({
      'Authorization': `Bearer ${localStorage.getItem('token')}`,
      'Accept':'application/json'
    });

    return headers;
  }

  getEvents(){
    return this.http.get(`${this.apiUrl}/event`);
  }

  searchEvents(query,page=''){
    return this.http.post(`${this.apiUrl}/event/search${page}`,query);
  }

  createdEvent(event){
    return this.http.post(`${this.apiUrl}/event`,event, { headers: this.getHeaders() });
  }

  addImageEvent(event_id,image){
    return this.http.post(`${this.apiUrl}/event/${event_id}/image`,image, { headers: this.getHeaders() });
  }

  getEvent(event_id){
    return this.http.get(`${this.apiUrl}/event/${event_id}`,{ headers: this.getHeaders() });
  }

  updateEvent(event,event_id){
    return this.http.put(`${this.apiUrl}/event/${event_id}`,event,{ headers: this.getHeaders() });
  }

  deleteEvent(event_id){
    return this.http.delete(`${this.apiUrl}/event/${event_id}`,{ headers: this.getHeaders() });
  }

  getEventsPopular(){
    return this.http.get(`${this.apiUrl}/event/popular`);
  }

  getEventsCreated(){
    return this.http.get(`${this.apiUrl}/event/my-events`,{ headers: this.getHeaders() });
  }

  getTypeEvent(){
    return this.http.get(`${this.apiUrl}/type-event`);
  }

  getDepartments(){
    return this.http.get(`${this.apiUrl}/utilities/department`);
  }

  getMinicipalities(department_id){
    return this.http.get(`${this.apiUrl}/utilities/department/${department_id}/municipalities`);
  }

  addEventFavorite(event){
    return this.http.post(`${this.apiUrl}/favorite`,event,{ headers: this.getHeaders() });
  }

  getEventsFavorites(){
    return this.http.get(`${this.apiUrl}/favorite`,{ headers: this.getHeaders() });
  }

  deleteFavoriteEvent(event_id){
    return this.http.delete(`${this.apiUrl}/favorite/${event_id}`,{ headers: this.getHeaders() });
  }

  addFavoriteTypeEvent(favorite){
    return this.http.post(`${this.apiUrl}/favorite-event-type`,{
      type_events:favorite
    },{ headers: this.getHeaders()})
  }

  getFavoriteTypeEvent(){
    return this.http.get(`${this.apiUrl}/favorite-event-type`,{ headers: this.getHeaders()});
  }

  buy_ticket(buy_ticket){
    return this.http.post(`${this.apiUrl}/ticket`,buy_ticket,{ headers: this.getHeaders()})
  }

  getTickets(){
    return this.http.get(`${this.apiUrl}/ticket`,{ headers: this.getHeaders()});
  }

  checkTicket(url){
    return this.http.get(url,{ headers: this.getHeaders()});
  }

  getCoordinatesAddress(query){
    return this.http.get(`https://api.mapbox.com/geocoding/v5/mapbox.places/${query}.json?access_token=${environment.apiKeyMap}`)
  }

}
