import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import { Router } from '@angular/router';
import { HttpClient, HttpHeaders } from '@angular/common/http';

// =========================================
// FIREBASE
// =========================================
import { AngularFireAuth } from '@angular/fire/auth';
import { auth } from 'firebase/app';

// =========================================
// URL - API
// =========================================
import { environment } from '../../../environments/environment';

// =========================================
// MODEL - USER
// =========================================
import { User } from '../../models/user';


@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private apiUrl = environment.apiUrl;
  private headers = new HttpHeaders();
  private intervalRefreshToken = null;
  user = null;

  constructor(
    private http: HttpClient,
    public authFirebase: AngularFireAuth,
    private router: Router
  ) {
    this.user  = JSON.parse(localStorage.getItem('user'));
    this.refresToken();
  }

  /**
   * permite iniciar session con un email y password
   * @param user
   */
  login(user: User) {
    return this.http.post(`${this.apiUrl}/auth/login`, user)
      .pipe(map((data: any) => {
        this.saveUser(data);
        this.refresToken();
        return user;
      }));
  }

  async loginFirebase(provider: string) {
    let authProvider = null;
    switch (provider) {
      case 'facebook':
        authProvider = new auth.FacebookAuthProvider();
        break;
      default:
        authProvider = new auth.GoogleAuthProvider();
        break;
    }
    return await this.loginSocial(authProvider);
  }

  async loginSocial(authProvider: auth.AuthProvider) {
    await this.authFirebase.auth.signInWithPopup(authProvider);
    const token = await this.authFirebase.auth.currentUser.getIdToken(true);
    return await this.http.get(`${this.apiUrl}/auth/firebase/${token}`).pipe(map((data: any) => {
      this.saveUser(data);
      this.refresToken();
      return data;
    })).toPromise();
  }

  /**
   * permite  registrar un usuario mediante emial y password
   * @param user 
   */
  register(user: User) {
    return this.http.post(`${this.apiUrl}/auth/register`, user)
      .pipe(map((data: any) => {
        this.saveUser(data);
        this.refresToken();
        return user;
      }));
  }

  /**
  * eliminar  de memoria el token y el usuario
  */
  logout() {
    localStorage.removeItem('token');
    localStorage.removeItem('user');
    localStorage.removeItem('expires');
    clearInterval(this.intervalRefreshToken);
    this.router.navigate(['/inicio']);
  }

  /**
   * permite guardar un usuario
   * @param data 
   */
  private saveUser(data: any) {
    const user = {
      ...data['user']
    };
    const hoy = new Date();
    hoy.setSeconds(data.expires_in);
    localStorage.setItem('expires', hoy.getTime().toString());
    localStorage.setItem('token', data.access_token);
    localStorage.setItem('user', JSON.stringify(data.user));
    this.user = user;
    return user;
  }

  /**
   * permite referscar el token cada 55 minutos
   */
  refresToken() {
    this.intervalRefreshToken = setInterval(() => {
      if (this.isLogin()) {
        const token = localStorage.getItem('token');
        this.headers = new HttpHeaders({
          Authorization: `Bearer ${token}`
        });
        this.http.get(`${this.apiUrl}/auth/refresh`, { headers: this.headers }).subscribe(
          (data: any) => {
            this.saveUser(data);
          }
        );
      } else {
        this.logout();
      }
    }, 1000 * 3300);
  }

  /**
   * verifica si el usuario esta autenticado y el token aun esta valido.
   */
  isLogin(): boolean {
    const token = localStorage.getItem('token');
    if (!token) {
      return false;
    }
    const expira = Number(localStorage.getItem('expires'));
    const expiraDate = new Date();
    expiraDate.setTime(expira);

    if (expiraDate > new Date()) {
      return true;
    }
    return false;
  }

  // This method allows to reset password
  resetPassword(email: string) {
    return this.http.post(`${this.apiUrl}/auth/password-reset`, email).pipe(map((res: any) => {
        return res;
    }));
  }

  /**
   * get roles of the user
   */
  roles(){
    const token = localStorage.getItem('token');
    this.headers = new HttpHeaders({
      Authorization: `Bearer ${token}`
    });
    return this.http.get(`${this.apiUrl}/auth/roles`, { headers: this.headers });
  }
}
