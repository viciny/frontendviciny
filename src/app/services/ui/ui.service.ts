import { Injectable } from '@angular/core';
import Swal from 'sweetalert2';

// =========================================
// UTILS
// =========================================
declare var $: any;

@Injectable({
  providedIn: 'root'
})
export class UiService {

  constructor() { }

   // This method is responsible for displaying success
   showSuccess(title: string) {
    Swal.fire({
        icon: 'success',
        title,
        showConfirmButton: false,
        showCancelButton: false
      });
  }

  // This method is responsible for displaying info
  showInfo(title: string) {
    Swal.fire({
        icon: 'info',
        title,
        showConfirmButton: false,
        showCancelButton: false
      });
  }

  // This method is responsible for displaying warning
  showWarning(title: string) {
    Swal.fire({
        icon: 'warning',
        title,
        showConfirmButton: false,
        showCancelButton: false
      });
  }

  // This method is responsible for displaying question
  showQuestion(title: string) {
    Swal.fire({
        icon: 'question',
        title,
        showConfirmButton: false,
        showCancelButton: false
      });
  }

  // This method is responsible for displaying error
  showError(err: any, title: string) {
    Swal.fire({
        icon: 'error',
        title,
        footer: this.typeErrors(err),
        showConfirmButton: false,
        showCancelButton: false
      });
  }

  // This method is responsible for displaying a loading
  loading() {
    Swal.fire({
      allowOutsideClick: false,
      icon: 'info',
      text: 'Espere por favor...'
    });
    Swal.showLoading();
  }

  // This method is responsible for closing a loading
  closeLoading() {
    Swal.close();
  }

  // This method is responsible for displaying errors
  private typeErrors(data: any): string {
    switch (data.status) {
      case 422:
        let messageError = '<ul>';
        for (const errors in data.error.errors) {
          if (data.error.errors.hasOwnProperty(errors)) {
            const error = data.error.errors[errors];
            let messages = `<li>${errors}:<ul>`;
            for (const message in error) {
              if (error.hasOwnProperty(message)) {
                messages += `<li> ${error[message]}</li>`;
              }
            }
            messageError += `${messages}</ul>`;
          }
        }
        return `${messageError}</ul>`;
      case 401:
        return `<ul>${data.error.error}</ul>`;
      case 403:
        return `<ul>${data.error.error}</ul>`;
      default:
        return '<ul>Por favor, comunicarse con el proveedor del servicio.</ul>';
    }
  }

  // This method is responsible for displaying tooltips
  showTooltips() {
    $('[data-toggle="tooltip"]').tooltip();
  }
}
