// =========================================
// MODULES
// =========================================
import { BrowserModule } from '@angular/platform-browser';
import { LOCALE_ID, NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ServicesModule } from './services/services.module';
import { PagesModule } from './pages/pages.module';

// =========================================
// ROUTES
// =========================================
import { AppRoutingModule } from './app-routing.module';

// =========================================
// PAGES
// =========================================
import { AppComponent } from './app.component';
import { RegisterComponent } from './auth/register/register.component';
import { LoginComponent } from './auth/login/login.component';

// =========================================
// LOCALE
// =========================================
import { registerLocaleData } from '@angular/common';
import localeEsCO from '@angular/common/locales/es-CO';


registerLocaleData(localeEsCO, 'es-Co');

@NgModule({
  declarations: [
    AppComponent,
    RegisterComponent,
    LoginComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    ServicesModule,
    PagesModule,
    AppRoutingModule
  ],
  providers: [
    {
      provide: LOCALE_ID,
      useValue: 'es-Co'
    },
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
