import { Component, OnInit } from '@angular/core';

import { AuthService } from '../../services/services.index';

@Component({
  selector: 'app-for-you',
  templateUrl: './for-you.component.html',
  styleUrls: ['./for-you.component.css']
})
export class ForYouComponent implements OnInit {

  isLogin: boolean;

  constructor(
    private authService: AuthService
  ) {
    this.isLogin = authService.isLogin();
  }

  ngOnInit() {
  }

}
