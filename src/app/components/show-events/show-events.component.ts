import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
// =========================================
// SERVICES
// =========================================
import {
  BusinessService,
  UiService,
  AuthService,
} from '../../services/services.index';

// =========================================
// MODEL - EVENT
// =========================================
import { Event } from '../../models/event';

@Component({
  selector: 'app-show-events',
  templateUrl: './show-events.component.html',
  styleUrls: ['./show-events.component.css']
})
export class ShowEventsComponent implements OnInit {

  @Input() events: Array<Event>;
  eventModal: Event;
  constructor(
    private businessService: BusinessService,
    private uiService: UiService,
    private authService: AuthService,
    private router: Router
  ) {}

  ngOnInit() {}

  // This method allows load to event selected
  openShared(event: Event) {
    this.eventModal = event;
  }

  // This method allows you to add an event to favorites
  addEventFavorite(event_id) {
    if (this.authService.isLogin()) {
      this.businessService.addEventFavorite({ event_id }).subscribe(res => {
        this.uiService.showSuccess('Viciny agregado a favoritos!');
      }, (err: any) => {
        if (err.status == 422) {
          this.uiService.showSuccess('Viciny ya esta en la lista de favoritos!');
        } else {
          this.uiService.showError(err, 'Error al agregar a favoritos');
        }
      });
    } else {
      this.router.navigate(['/login']);
    }
  }

}
