import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-share-social',
  templateUrl: './share-social.component.html',
  styleUrls: ['./share-social.component.css']
})
export class ShareSocialComponent implements OnInit {

  @Input() eventModal: Event;

  constructor() {
  }

  ngOnInit() {
  }
}
