import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';

// =========================================
// URL - APIKEYMAP
// =========================================
import { environment } from '../../../environments/environment';

// =========================================
// SERVICES
// =========================================
import {
  UserService,
  AuthService,
  BusinessService,
  UiService
} from '../../services/services.index';

// =========================================
// UTILITIES
// =========================================
declare var ePayco: any;

@Component({
  selector: 'app-modal-pay',
  templateUrl: './modal-pay.component.html',
  styleUrls: ['./modal-pay.component.css']
})
export class ModalPayComponent implements OnInit {

  @Input() event;

  tickes_quantity = 1;

  constructor(
    private authService: AuthService,
    private userService: UserService,
    private bussinesService: BusinessService,
    private uiService: UiService,
    private router: Router,
  ) { }

  ngOnInit() {
  }

  buy() {
    if (this.authService.isLogin()) {
      let buy_ticket = {
        event_id: this.event.id,
        tickes_quantity: this.tickes_quantity
      };

      this.uiService.loading();
      this.bussinesService.buy_ticket(buy_ticket).subscribe((res: any) => {
          this.uiService.closeLoading();
          if (res.paid) {
            this.router.navigate(['/mis-llaves']);
          } else {
            this.buy_ticket(res.invoice);
          }
        }, err => {
          this.uiService.showError(err, 'Error al cargar viciny');
        });
    } else {
      this.router.navigate(['/login']);
    }
  }

  buy_ticket(invoice) {
    const handler = ePayco.checkout.configure(environment.epayco);
    const user: any = this.userService.user;
    const total = this.tickes_quantity * this.event.ticket_value;
    const total_iva = total + total * 0.19;

    const data = {
      invoice: invoice,
      // Parametros compra (obligatorio)
      name: 'Viciny: ' + this.event.name,
      description: 'Viciny: ' + this.event.name + '. Cantidad de llaves ' + this.tickes_quantity,
      currency: 'cop',
      amount: total_iva,
      tax_base: total,
      tax: total * 0.19,
      country: 'co',
      lang: 'es',
      // Onpage = 'false' - Standard = 'true'
      external: 'false',
      //Atributos cliente
      name_billing: user.name,
      email_billing: user.email,
      // Atributos opcionales
      confirmation: environment.apiUrl + '/ticket/confirmation-pay',
      acepted: environment.url + '/estado-de-la-compra/Aceptada',
      rejected: environment.url + '/estado-de-la-compra/Rechazada',
      pending: environment.url + '/estado-de-la-compra/Pendiente',
    };
    handler.open(data);
  }
}
