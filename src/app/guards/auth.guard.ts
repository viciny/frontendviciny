import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';

// =========================================
// SERVICES
// =========================================
import {
  AuthService
} from '../services/services.index';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {

  constructor(
    private authService: AuthService,
    private router: Router
  ) {}

  canActivate() {
    if (this.authService.isLogin()) {
      return true;
    } else {
      this.router.navigate(['/login']);
      return false;
    }
  }
}
