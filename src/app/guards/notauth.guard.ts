import { Injectable } from '@angular/core';
import { CanActivate, Router} from '@angular/router';
import { Observable } from 'rxjs';

// =========================================
// SERVICES
// =========================================
import {
  AuthService
} from '../services/services.index';

@Injectable({
  providedIn: 'root'
})
export class NotauthGuard implements CanActivate {

  constructor(
    private authService: AuthService,
    private router: Router
  ) {}

  canActivate() {
    if (!this.authService.isLogin()) {
      return true;
    } else {
      this.router.navigate(['/']);
      return false;
    }
  }
}
