export class Profile {
    type_document: string = 'cc';
    nit: string = '';
    photo: string = '';
    phone: string = '';
    address: string = '';
    bank_name: string = '';
    account_bank: string = '';
    user_id: string = '';
    municipality: any = null;
    created_at: string = '';
    updated_at: string = '';
}
