import { Profile } from './profile';
export class User {
    name: string = '';
    email: string = '';
    email_verified_at: string = '';
    created_at: string = '';
    updated_at: string = '';
    profile: Profile = new Profile();
}
