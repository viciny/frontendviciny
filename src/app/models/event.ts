export class Event {
    address: string;
    category_of_event_id: number;
    created_at: Date;
    description: string;
    finish_date: Date;
    id: number;
    image: string;
    name: string;
    municipality: any;
    tickets: number;
    ticket_value: number;
    total:number;
    numberTickets:number;
    organizer: string;
    start_date: Date;
    type_event_id: number;
    type_event: any;
    updated_at: Date;
    url_youtube: string;
    edit: any;
    delete: any;
}
