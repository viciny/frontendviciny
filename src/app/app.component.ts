import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { ActivationEnd, Router } from '@angular/router';
import { filter, map } from 'rxjs/operators';

// =========================================
// UTILITIES
// =========================================
declare function init_all();

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

  constructor(
    private titleRoute: Title,
    private router: Router
  ) {
    init_all();
    this.getDataRoute().subscribe(res => {
      this.titleRoute.setTitle('Viciny - ' + res.title);
    });
  }

  ngOnInit() {
    init_all();
  }

  getDataRoute() {
    return this.router.events.pipe(
      filter(event => event instanceof ActivationEnd),
      filter((event: ActivationEnd) => event.snapshot.firstChild === null),
      map((event: ActivationEnd) => event.snapshot.data)
    );
  }
}
